package ru.t1.panasyuk.tm.service;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IdEmptyException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.model.Session;
import ru.t1.panasyuk.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@DisplayName("Тестирование сервиса SessionService")
public class SessionServiceTest {

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IUserService userService;

    @NotNull
    private User test;

    @NotNull
    private User admin;

    @NotNull
    private User user;

    @NotNull
    private static IConnectionService connectionService;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() throws Exception {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);
        userService = new UserService(propertyService, connectionService, projectTaskService);
        sessionService = new SessionService(connectionService);
        test = userService.create("TEST", "TEST", "TEST@TEST.ru");
        admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        user = userService.create("USER", "USER", Role.ADMIN);
        @NotNull final Session session1 = new Session();
        session1.setUserId(test.getId());
        session1.setRole(test.getRole());
        @NotNull final Session session2 = new Session();
        session2.setUserId(admin.getId());
        session2.setRole(admin.getRole());
        @NotNull final Session session3 = new Session();
        session3.setUserId(user.getId());
        session3.setRole(user.getRole());
        sessionList = new ArrayList<>();
        sessionList.add(session1);
        sessionList.add(session2);
        sessionList.add(session3);
        sessionService.add(sessionList);
    }

    @After
    public void afterTest() throws Exception {
        sessionService.clear(test.getId());
        sessionService.clear(user.getId());
        sessionService.clear(admin.getId());
        userService.remove(admin);
        userService.remove(test);
        userService.remove(user);
    }

    @Test
    @DisplayName("Добавление сессии")
    public void AddTest() throws Exception {
        int expectedNumberOfEntries = sessionService.getSize() + 1;
        @NotNull final Session session = new Session();
        session.setUserId(test.getId());
        session.setRole(Role.USUAL);
        sessionService.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    @DisplayName("Добавление сессии для пользователя")
    public void AddForUserTest() throws Exception {
        int expectedNumberOfEntries = sessionService.getSize(test.getId()) + 1;
        @NotNull final Session session = new Session();
        session.setUserId(test.getId());
        session.setRole(test.getRole());
        sessionService.add(test.getId(), session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Добавление Null сессии для пользователя")
    public void AddNullForUserTest() throws Exception {
        int expectedNumberOfEntries = sessionService.getSize(test.getId());
        @Nullable final Session session = sessionService.add(test.getId(), null);
        Assert.assertNull(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Добавление списка сессий")
    public void AddCollectionTest() throws Exception {
        int expectedNumberOfEntries = sessionService.getSize() + 2;
        @NotNull final List<Session> sessionList = new ArrayList<>();
        @NotNull final Session firstSession = new Session();
        firstSession.setUserId(test.getId());
        firstSession.setRole(Role.USUAL);
        sessionList.add(firstSession);
        @NotNull final Session secondSession = new Session();
        secondSession.setUserId(user.getId());
        secondSession.setRole(Role.USUAL);
        sessionList.add(secondSession);
        sessionService.add(sessionList);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    @DisplayName("Удалить все сессии для пользователя")
    public void clearForUserTest() throws Exception {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(sessionService.getSize(test.getId()) > 0);
        sessionService.clear(test.getId());
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Проверить существование сессии по Id")
    public void existByIdTrueTest() throws Exception {
        for (@NotNull final Session session : sessionList) {
            final boolean isExist = sessionService.existsById(session.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    @DisplayName("Проверить несуществование сессии по Id")
    public void existByIdFalseTest() throws Exception {
        final boolean isExist = sessionService.existsById("123321");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Проверить существование сессии по Id для пользователя")
    public void existByIdTrueForUserTest() throws Exception {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessionsForTestUser) {
            final boolean isExist = sessionService.existsById(test.getId(), session.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    @DisplayName("Проверить несуществование сессии по Id для пользователя")
    public void existByIdFalseUserTest() throws Exception {
        final boolean isExist = sessionService.existsById("45", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Найти все сессии")
    public void findAllTest() throws Exception {
        @NotNull final List<Session> sessions = sessionService.findAll();
        Assert.assertTrue(sessions.size() > 0);
    }

    @Test
    @DisplayName("Найти все сессии для пользователя")
    public void findAllForUserTest() throws Exception {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Session> sessions = sessionService.findAll(test.getId());
        Assert.assertEquals(sessionsForTestUser.size(), sessions.size());
    }

    @Test
    @DisplayName("Найти сессию по Id")
    public void findOneByIdTest() throws Exception {
        @Nullable Session foundSession;
        for (@NotNull final Session session : sessionList) {
            foundSession = sessionService.findOneById(session.getId());
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    @DisplayName("Найти сессию по Id для пользователя")
    public void findOneByIdForUserTest() throws Exception {
        @Nullable Session foundSession;
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessionsForTestUser) {
            foundSession = sessionService.findOneById(test.getId(), session.getId());
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    @DisplayName("Найти сессию по Null Id")
    public void findOneByIdNullTest() throws Exception {
        @Nullable final Session foundSession = sessionService.findOneById(null);
        Assert.assertNull(foundSession);
    }

    @Test
    @DisplayName("Найти сессию по пустому Id")
    public void findOneByIdEmptyTest() throws Exception {
        @Nullable final Session foundSession = sessionService.findOneById("");
        Assert.assertNull(foundSession);
    }

    @Test
    @DisplayName("Найти сессию по Null Id для пользователя")
    public void findOneByIdNullForUserTest() throws Exception {
        @Nullable final Session foundSession = sessionService.findOneById(test.getId(), null);
        Assert.assertNull(foundSession);
    }

    @Test
    @DisplayName("Найти сессию по индексу")
    public void findOneByIndexTest() throws Exception {
        for (int i = 1; i <= sessionList.size(); i++) {
            @Nullable final Session session = sessionService.findOneByIndex(i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    @DisplayName("Найти сессию по индексу для пользователя")
    public void findOneByIndexForUserTest() throws Exception {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= sessionsForTestUser.size(); i++) {
            @Nullable final Session session = sessionService.findOneByIndex(test.getId(), i);
            Assert.assertNotNull(session);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти сессию по индексу превышающему число сессий для пользователя")
    public void findOneByIndexForUserIndexIncorrectNegative() throws Exception {
        int index = sessionService.getSize(test.getId()) + 1;
        @Nullable final Session session = sessionService.findOneByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти сессию по Null индексу для пользователя")
    public void findOneByIndexForUserNullIndexIncorrectNegative() throws Exception {
        @Nullable final Session session = sessionService.findOneByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти сессию по отрицательному индексу для пользователя")
    public void findOneByIndexForUserMinusIndexIncorrectNegative() throws Exception {
        @Nullable final Session session = sessionService.findOneByIndex(test.getId(), -1);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти сессию по индексу превышающему количество сессий")
    public void findOneByIndexIndexIncorrectNegative() throws Exception {
        int index = sessionService.getSize() + 1;
        @Nullable final Session session = sessionService.findOneByIndex(index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти сессию по Null индексу")
    public void findOneByIndexNullIndexIncorrectNegative() throws Exception {
        @Nullable final Session session = sessionService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти сессию по отрицательному индексу")
    public void findOneByIndexMinusIndexIncorrectNegative() throws Exception {
        @Nullable final Session session = sessionService.findOneByIndex(-1);
    }

    @Test
    @DisplayName("Получить количество сессий")
    public void getSizeTest() throws Exception {
        int actualSize = sessionService.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    @DisplayName("Получить количество сессий для пользователя")
    public void getSizeForUserTest() throws Exception {
        int expectedSize = (int) sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .count();
        int actualSize = sessionService.getSize(test.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удалить сессию")
    public void removeTest() throws Exception {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionService.remove(session);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить Null сессию")
    public void removeEntityNullNotFoundTestNegative() throws Exception {
        @Nullable final Session deletedSession = sessionService.remove(null);
    }

    @Test
    @DisplayName("Удалить все сессии методом RemoveAll")
    public void removeAllTest() throws Exception {
        int expectedNumberOfEntries = 0;
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        sessionService.removeAll(sessionsForTestUser);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Удалить сессию по Id")
    public void removeByIdTest() throws Exception {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionService.removeById(sessionId);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test
    @DisplayName("Удалить сессию по Id для пользователя")
    public void removeByIdForUserTest() throws Exception {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessionsForTestUser) {
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionService.removeById(test.getId(), sessionId);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(test.getId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить сессию по Null Id")
    public void removeByIdIdNullTestNegative() throws Exception {
        sessionService.removeById(null);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить сессию по пустому Id")
    public void removeByIdIdEmptyTestNegative() throws Exception {
        sessionService.removeById("");
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить несуществующую сессию по Id")
    public void removeByIdEntityNotFoundTestNegative() throws Exception {
        sessionService.removeById("123321");
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить сессию по Null Id для пользователя")
    public void removeByIdForUserIdNullTestNegative() throws Exception {
        sessionService.removeById(test.getId(), null);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить сессию по пустому Id для пользователя")
    public void removeByIdForUserIdEmptyTestNegative() throws Exception {
        sessionService.removeById(test.getId(), "");
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить несуществующую сессию по Id для пользователя")
    public void removeByIdForUserEntityNotFoundTestNegative() throws Exception {
        sessionService.removeById(test.getId(), "123321");
    }

    @Test
    @DisplayName("Удалить сессию по индексу для пользователя")
    public void removeByIndexForUserTest() throws Exception {
        int index = (int) sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Session deletedSession = sessionService.removeByIndex(test.getId(), index);
            Assert.assertNotNull(deletedSession);
            @NotNull final String sessionId = deletedSession.getId();
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(test.getId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить сессию по индексу превышающему количество сессий")
    public void removeByIndexIndexIncorrectTestNegative() throws Exception {
        int index = sessionList.size() + 1;
        sessionService.removeByIndex(index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить сессию по Null индексу")
    public void removeByIndexNullIndexIncorrectTestNegative() throws Exception {
        sessionService.removeByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить сессию по отрицательному индексу")
    public void removeByIndexMinusIndexIncorrectTestNegative() throws Exception {
        sessionService.removeByIndex(-1);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить сессию по индексу превышающему количество сессий для пользователя")
    public void removeByIndexForUserIndexIncorrectTestNegative() throws Exception {
        int index = sessionList.size() + 1;
        sessionService.removeByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить сессию по Null индексу для пользователя")
    public void removeByIndexNullForUserIndexIncorrectTestNegative() throws Exception {
        sessionService.removeByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить сессию по отрицательному индексу для пользователя")
    public void removeByIndexMinusForUserIndexIncorrectTestNegative() throws Exception {
        sessionService.removeByIndex(test.getId(), -1);
    }

}