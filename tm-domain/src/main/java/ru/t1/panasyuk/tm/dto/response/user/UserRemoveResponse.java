package ru.t1.panasyuk.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public final class UserRemoveResponse extends AbstractUserResponse {

    @Nullable
    private User user;

    public UserRemoveResponse(@Nullable final User user) {
        this.user = user;
    }

    public UserRemoveResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}