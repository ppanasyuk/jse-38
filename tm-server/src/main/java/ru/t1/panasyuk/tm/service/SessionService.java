package ru.t1.panasyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.repository.ISessionRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.ISessionService;
import ru.t1.panasyuk.tm.model.Session;
import ru.t1.panasyuk.tm.repository.SessionRepository;

import java.sql.Connection;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    @Override
    public ISessionRepository getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}