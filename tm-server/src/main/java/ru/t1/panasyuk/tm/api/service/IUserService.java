package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.model.User;

public interface IUserService extends IService<User>, IUserRepository {

    @NotNull
    User create(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable String email) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull Role role) throws Exception;

    void lockUserByLogin(@Nullable String login) throws Exception;

    @NotNull
    User removeByLogin(@Nullable String login) throws Exception;

    @NotNull
    User removeByEmail(@Nullable String email) throws Exception;

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;

    @NotNull
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) throws Exception;

}