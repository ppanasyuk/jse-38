package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.model.User;

public interface IAuthService {

    void checkRoles(@Nullable Role[] roles);

    User check(@Nullable String login, @Nullable String password);

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @Nullable String email);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    User getUser();

}