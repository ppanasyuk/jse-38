package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.Session;
import ru.t1.panasyuk.tm.model.User;

public interface IAuthService {

    User check(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    void logout(@Nullable String token);

    @NotNull
    Session validateToken(@Nullable String token);

    void invalidate(@Nullable Session session) throws Exception;

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @Nullable String email) throws Exception;

}