package ru.t1.panasyuk.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public final class UserUpdateProfileResponse extends AbstractUserResponse {

    @Nullable
    private User user;

    public UserUpdateProfileResponse(@Nullable final User user) {
        this.user = user;
    }

    public UserUpdateProfileResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}