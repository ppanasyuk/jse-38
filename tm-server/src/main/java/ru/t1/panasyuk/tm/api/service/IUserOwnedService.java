package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.IUserOwnedRepository;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable Sort sort) throws Exception;

}