package ru.t1.panasyuk.tm.repository;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.service.ConnectionService;
import ru.t1.panasyuk.tm.service.PropertyService;
import ru.t1.panasyuk.tm.util.HashUtil;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@DisplayName("Тестирование репозитория для пользователей")
public class UserRepositoryTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private Connection connection;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initRepository() throws Exception {
        connection = connectionService.getConnection();
        userList = new ArrayList<>();
        userRepository = new UserRepository(connection);
        try {
            for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
                @NotNull final User user = new User();
                user.setLogin("USER" + i);
                user.setEmail("USER@" + i);
                user.setFirstName("User" + i);
                user.setLastName("User LastName" + i);
                user.setPasswordHash(HashUtil.salt(new PropertyService(), "qwerty" + i));
                userRepository.add(user);
                userList.add(user);
            }
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @After
    public void closeConnection() throws Exception {
        for (@NotNull final User user : userList) {
            userRepository.remove(user);
        }
        connection.commit();
        connection.close();
    }

    @Test
    @DisplayName("Добавление пользователя")
    public void addTest() throws Exception {
        int expectedNumberOfEntries = userRepository.getSize() + 1;
        @NotNull final String userFirstName = "User FirstName";
        @NotNull final String userLastName = "User LastName";
        @NotNull final String userLogin = "User Login";
        @NotNull final String userEmail = "User Description";
        @NotNull final User user = new User();
        user.setFirstName(userFirstName);
        user.setLastName(userLastName);
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        user.setPasswordHash("123");
        userRepository.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        @Nullable final User createdUser = userRepository.findOneById(user.getId());
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(userFirstName, createdUser.getFirstName());
        Assert.assertEquals(userLastName, createdUser.getLastName());
        Assert.assertEquals(userLogin, createdUser.getLogin());
        Assert.assertEquals(userEmail, createdUser.getEmail());
        userRepository.remove(createdUser);
    }

    @Test
    @DisplayName("Добавление списка пользователей")
    public void addAllTest() throws Exception {
        int expectedNumberOfEntries = userRepository.getSize() + 2;
        @NotNull final List<User> users = new ArrayList<>();
        @NotNull final String firstUserFirstName = "User 1 FirstName";
        @NotNull final String firstUserLastName = "User 1 LastName";
        @NotNull final String firstUserLogin = "User 1 Login";
        @NotNull final String firstUserEmail = "User 1 Description";
        @NotNull final User firstUser = new User();
        firstUser.setFirstName(firstUserFirstName);
        firstUser.setLastName(firstUserLastName);
        firstUser.setLogin(firstUserLogin);
        firstUser.setEmail(firstUserEmail);
        firstUser.setPasswordHash("321");
        users.add(firstUser);
        @NotNull final String secondUserFirstName = "User 2 FirstName";
        @NotNull final String secondUserLastName = "User 2 LastName";
        @NotNull final String secondUserLogin = "User 2 Login";
        @NotNull final String secondUserEmail = "User 2 Description";
        @NotNull final User secondUser = new User();
        secondUser.setFirstName(secondUserFirstName);
        secondUser.setLastName(secondUserLastName);
        secondUser.setLogin(secondUserLogin);
        secondUser.setEmail(secondUserEmail);
        secondUser.setPasswordHash("342");
        users.add(secondUser);
        @NotNull final Collection<User> addedUsers = userRepository.add(users);
        Assert.assertTrue(addedUsers.size() > 0);
        int actualNumberOfEntries = userRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
        userRepository.remove(firstUser);
        userRepository.remove(secondUser);
    }

    @Test
    @DisplayName("Проверка существования пользователя по Id")
    public void existByIdTrueTest() throws Exception {
        boolean expectedResult = true;
        @Nullable final User user = userRepository.findOneByIndex(1);
        Assert.assertNotNull(user);
        @Nullable final String userId = user.getId();
        Assert.assertEquals(expectedResult, userRepository.existsById(userId));
    }

    @Test
    @DisplayName("Проверка несуществования пользователя по Id")
    public void existByIdFalseTest() throws Exception {
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, userRepository.existsById("qwerty"));
    }

    @Test
    @DisplayName("Поиск всех пользователей")
    public void findAllTest() throws Exception {
        @NotNull final List<User> users = userRepository.findAll();
        Assert.assertTrue(users.size() > 0);
    }

    @Test
    @DisplayName("Поиск пользователя по логину")
    public void findByLoginTest() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            @Nullable final User foundUser = userRepository.findByLogin(login);
            Assert.assertEquals(user, foundUser);
        }
    }

    @Test
    @DisplayName("Поиск пользователя по Null логину")
    public void findByLoginNullTest() throws Exception {
        @Nullable final User foundUserNull = userRepository.findByLogin(null);
        Assert.assertNull(foundUserNull);
        @Nullable final User foundUser = userRepository.findByLogin("123321");
        Assert.assertNull(foundUser);
    }

    @Test
    @DisplayName("Поиск пользователя по Email")
    public void findByEmailTest() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            @Nullable final User foundUser = userRepository.findByEmail(email);
            Assert.assertEquals(user, foundUser);
        }
    }

    @Test
    @DisplayName("Поиск пользователя по Null Email")
    public void findByEmailNullTest() throws Exception {
        @Nullable final User foundUserNull = userRepository.findByEmail(null);
        Assert.assertNull(foundUserNull);
        @Nullable final User foundUser = userRepository.findByEmail("123321");
        Assert.assertNull(foundUser);
    }

    @Test
    @DisplayName("Поиск пользователя по Id")
    public void findOneByIdTest() throws Exception {
        @Nullable User user;
        for (int i = 0; i < userList.size(); i++) {
            user = userList.get(i);
            Assert.assertNotNull(user);
            @NotNull final String userId = user.getId();
            @Nullable final User foundUser = userRepository.findOneById(userId);
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    @DisplayName("Поиск пользователя по Null Id")
    public void findOneByIdNullTest() throws Exception {
        @Nullable final User foundUser = userRepository.findOneById("qwerty");
        Assert.assertNull(foundUser);
        @Nullable final User foundUserNull = userRepository.findOneById(null);
        Assert.assertNull(foundUserNull);
    }

    @Test
    @DisplayName("Поиск пользователя по индексу")
    public void findOneByIndexTest() throws Exception {
        for (int i = 1; i <= userList.size(); i++) {
            @Nullable final User user = userRepository.findOneByIndex(i);
            Assert.assertNotNull(user);
        }
    }

    @Test
    @DisplayName("Поиск пользователя по Null индексу")
    public void findOneByIndexNullTest() throws Exception {
        @Nullable final User user = userRepository.findOneByIndex(null);
        Assert.assertNull(user);
    }

    @Test
    @DisplayName("Получение количества пользователей")
    public void getSizeTest() throws Exception {
        int actualSize = userRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    @DisplayName("Проверка существования Email в репозитории")
    public void isEmailExistTest() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            final boolean isEmailExists = userRepository.isEmailExist(email);
            Assert.assertTrue(isEmailExists);
        }
    }

    @Test
    @DisplayName("Проверка существования Null Email в репозитории")
    public void isEmailExistNullTest() throws Exception {
        final boolean isEmailExistsNull = userRepository.isEmailExist(null);
        Assert.assertFalse(isEmailExistsNull);
        final boolean isEmailExists = userRepository.isEmailExist("123321");
        Assert.assertFalse(isEmailExists);
    }

    @Test
    @DisplayName("Проверка существования логина в репозитории")
    public void isLoginExistTest() throws Exception {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            final boolean isLoginExists = userRepository.isLoginExist(login);
            Assert.assertTrue(isLoginExists);
        }
    }

    @Test
    @DisplayName("Проверка существования Null логина в репозитории")
    public void isLoginExistsNull() throws Exception {
        final boolean isLoginExistsNull = userRepository.isLoginExist(null);
        Assert.assertFalse(isLoginExistsNull);
        final boolean isLoginExists = userRepository.isLoginExist("123321");
        Assert.assertFalse(isLoginExists);
    }

    @Test
    @DisplayName("Удаление пустого списка пользователей")
    public void removeAllNullTest() throws Exception {
        int expectedNumberOfEntries = userRepository.getSize();
        userRepository.removeAll(null);
        int actualNumberOfEntries = userRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    @DisplayName("Удаление пользователя")
    public void removeTest() throws Exception {
        @Nullable final User user = userList.get(1);
        Assert.assertNotNull(user);
        @NotNull final String userId = user.getId();
        @Nullable final User deletedUser = userRepository.remove(user);
        Assert.assertNotNull(deletedUser);
        @Nullable final User deletedUserInRepository = userRepository.findOneById(userId);
        Assert.assertNull(deletedUserInRepository);
    }

    @Test
    @DisplayName("Удаление Null пользователя")
    public void removeNullTest() throws Exception {
        @Nullable final User user = userRepository.remove(null);
        Assert.assertNull(user);
    }

    @Test
    @DisplayName("Удаление пользователя по Id")
    public void removeByIdTest() throws Exception {
        int index = userList.size();
        while (index > 0) {
            @Nullable final User user = userList.get(index - 1);
            Assert.assertNotNull(user);
            @NotNull final String userId = user.getId();
            @Nullable final User deletedUser = userRepository.removeById(userId);
            Assert.assertNotNull(deletedUser);
            @Nullable final User deletedUserInRepository = userRepository.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
            index--;
        }
    }

    @Test
    @DisplayName("Удаление пользователя по Null Id")
    public void removeByIdNullTest() throws Exception {
        @Nullable final User deletedUser = userRepository.removeById("qwerty");
        Assert.assertNull(deletedUser);
        @Nullable final User deletedUserNull = userRepository.removeById(null);
        Assert.assertNull(deletedUserNull);
    }

}