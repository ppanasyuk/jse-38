package ru.t1.panasyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.constant.DBConst;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.model.Task;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConst.TABLE_TASK;
    }

    @NotNull
    @Override
    protected String getColumns() {
        return String.format(
                "%s, %s, %s, %s, %s, %s, %s",
                DBConst.COLUMN_ID,
                DBConst.COLUMN_NAME,
                DBConst.COLUMN_CREATED,
                DBConst.COLUMN_STATUS,
                DBConst.COLUMN_DESCRIPTION,
                DBConst.COLUMN_PROJECT_ID,
                DBConst.COLUMN_USER_ID
        );
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) throws Exception {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? AND %s = ?",
                getTableName(),
                DBConst.COLUMN_USER_ID,
                DBConst.COLUMN_PROJECT_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @Override
    public void removeAllByProjectId(@NotNull String userId, @Nullable String projectId) throws Exception {
        @NotNull final String sql = String.format(
                "DELETE FROM %s WHERE %s = ? AND %s = ?",
                getTableName(),
                DBConst.COLUMN_USER_ID,
                DBConst.COLUMN_PROJECT_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    public Task add(@NotNull final Task task) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s) VALUES(?, ?, ?, ?, ?, ?, ?)",
                getTableName(),
                getColumns()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setString(2, task.getName());
            statement.setTimestamp(3, new Timestamp(task.getCreated().getTime()));
            statement.setString(4, task.getStatus().toString());
            statement.setString(5, task.getDescription());
            statement.setString(6, task.getProjectId());
            statement.setString(7, task.getUserId());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    public Task fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(row.getString(DBConst.COLUMN_ID));
        task.setName(row.getString(DBConst.COLUMN_NAME));
        task.setCreated(row.getTimestamp(DBConst.COLUMN_CREATED));
        task.setStatus(Status.valueOf(row.getString(DBConst.COLUMN_STATUS)));
        task.setDescription(row.getString(DBConst.COLUMN_DESCRIPTION));
        task.setProjectId(row.getString(DBConst.COLUMN_PROJECT_ID));
        task.setUserId(row.getString(DBConst.COLUMN_USER_ID));
        return task;
    }

    @Override
    public void update(@NotNull final Task task) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(),
                DBConst.COLUMN_NAME,
                DBConst.COLUMN_DESCRIPTION,
                DBConst.COLUMN_STATUS,
                DBConst.COLUMN_PROJECT_ID,
                DBConst.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().toString());
            statement.setString(4, task.getProjectId());
            statement.setString(5, task.getId());
            statement.executeUpdate();
        }
    }

}