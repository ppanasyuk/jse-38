package ru.t1.panasyuk.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.endpoint.IAuthEndpoint;
import ru.t1.panasyuk.tm.api.service.IAuthService;
import ru.t1.panasyuk.tm.api.service.IServiceLocator;
import ru.t1.panasyuk.tm.dto.request.user.UserLoginRequest;
import ru.t1.panasyuk.tm.dto.request.user.UserLogoutRequest;
import ru.t1.panasyuk.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.panasyuk.tm.dto.response.user.UserLoginResponse;
import ru.t1.panasyuk.tm.dto.response.user.UserLogoutResponse;
import ru.t1.panasyuk.tm.dto.response.user.UserViewProfileResponse;
import ru.t1.panasyuk.tm.model.Session;
import ru.t1.panasyuk.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.panasyuk.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse loginUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token;
        try {
            token = authService.login(request.getLogin(), request.getPassword());
        } catch (@NotNull final Exception e) {
            return new UserLoginResponse(e);
        }
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    public UserLogoutResponse logoutUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        authService.logout(request.getToken());
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserViewProfileResponse viewUserProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserViewProfileRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user;
        try {
            user = getServiceLocator().getUserService().findOneById(userId);
        } catch (@NotNull final Exception e) {
            return new UserViewProfileResponse(e);
        }
        return new UserViewProfileResponse(user);
    }

}