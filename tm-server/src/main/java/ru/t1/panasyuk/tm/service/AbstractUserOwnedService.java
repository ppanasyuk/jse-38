package ru.t1.panasyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.IRepository;
import ru.t1.panasyuk.tm.api.repository.IUserOwnedRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IUserOwnedService;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IdEmptyException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    @NotNull
    @Override
    public abstract IUserOwnedRepository<M> getRepository(@NotNull final Connection connection);

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Nullable
    @Override
    public M add(@NotNull final String userId, @Nullable final M model) throws Exception {
        if (model == null) return null;
        model.setUserId(userId);
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                repository.add(model);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return model;
    }

    @Override
    public void clear(@NotNull final String userId) throws Exception {
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
                repository.clear(userId);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) throws Exception {
        boolean result;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
                result = repository.existsById(userId, id);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) throws Exception {
        @NotNull final List<M> models;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
                models = repository.findAll(userId);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) throws Exception {
        @NotNull final List<M> models;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
                models = repository.findAll(userId, comparator);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return models;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @Nullable final String id) throws Exception {
        if (id == null) return null;
        @Nullable final M model;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
                model = repository.findOneById(userId, id);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return model;
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @Nullable final Integer index) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final M model;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
                model = repository.findOneByIndex(userId, index);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return model;
    }

    @Override
    public int getSize(@NotNull final String userId) throws Exception {
        int result;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
                result = repository.getSize(userId);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    public M removeById(@NotNull final String userId, @Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M removedModel;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
                removedModel = repository.removeById(userId, id);
                if (removedModel == null) throw new EntityNotFoundException();
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return removedModel;
    }

    @NotNull
    @Override
    public M removeByIndex(@NotNull final String userId, @Nullable final Integer index) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final M removedModel;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
                removedModel = repository.removeByIndex(userId, index);
                if (removedModel == null) throw new EntityNotFoundException();
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return removedModel;
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<M> findAll(@NotNull final String userId, @Nullable final Sort sort) throws Exception {
        if (sort == null) return findAll(userId);
        final Comparator<M> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

}