package ru.t1.panasyuk.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.User;

import java.sql.SQLException;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login) throws Exception;

    @Nullable
    User findByEmail(@Nullable String email) throws Exception;

    @NotNull
    Boolean isLoginExist(@Nullable String login) throws Exception;

    @NotNull
    Boolean isEmailExist(@Nullable String email) throws Exception;

}