package ru.t1.panasyuk.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public final class TaskStartByIndexResponse extends AbstractTaskResponse {

    @Nullable
    private Task task;

    public TaskStartByIndexResponse(@Nullable final Task task) {
        this.task = task;
    }

    public TaskStartByIndexResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}