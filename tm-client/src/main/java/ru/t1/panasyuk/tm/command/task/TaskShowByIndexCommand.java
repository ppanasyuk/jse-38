package ru.t1.panasyuk.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.task.TaskFindOneByIndexRequest;
import ru.t1.panasyuk.tm.dto.response.task.TaskFindOneByIndexResponse;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Show task by index.";

    @NotNull
    private static final String NAME = "task-show-by-index";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final TaskFindOneByIndexRequest request = new TaskFindOneByIndexRequest(getToken(), index);
        @NotNull final TaskFindOneByIndexResponse response = getTaskEndpoint().findTaskByIndex(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        @Nullable final Task task = response.getTask();
        showTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}