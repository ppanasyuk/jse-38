package ru.t1.panasyuk.tm.repository;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.IRepository;
import ru.t1.panasyuk.tm.api.repository.IUserOwnedRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.service.ConnectionService;
import ru.t1.panasyuk.tm.service.PropertyService;
import ru.t1.panasyuk.tm.util.SystemUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@DisplayName("Тестирование репозитория проектов")
public class ProjectRepositoryTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String USER_1_ID = SystemUtil.generateGuid();

    @NotNull
    private final String USER_2_ID = SystemUtil.generateGuid();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private Connection connection;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initRepository() throws Exception {
        projectList = new ArrayList<>();
        connection = connectionService.getConnection();
        try {
            projectRepository = new ProjectRepository(connection);
            for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
                @NotNull final Project project = new Project();
                project.setName("Project " + i);
                project.setDescription("Description " + i);
                if (i < 5) project.setUserId(USER_1_ID);
                else project.setUserId(USER_2_ID);
                projectList.add(project);
                projectRepository.add(project);
            }
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @After
    public void closeConnection() throws Exception {
        projectRepository.clear(USER_1_ID);
        projectRepository.clear(USER_2_ID);
        connection.commit();
        connection.close();
    }

    @Test
    @DisplayName("Добавление проекта")
    public void addTest() throws Exception {
        int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final String projectName = "Project Name";
        @NotNull final String projectDescription = "Project Description";
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        @Nullable final Project createdProject = projectRepository.add(USER_1_ID, project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(USER_1_ID, createdProject.getUserId());
        Assert.assertEquals(projectName, createdProject.getName());
        Assert.assertEquals(projectDescription, createdProject.getDescription());
    }

    @Test
    @DisplayName("Добавление Null проекта")
    public void addNullTest() throws Exception {
        @Nullable final Project createdProject = projectRepository.add(USER_1_ID, null);
        Assert.assertNull(createdProject);
    }

    @Test
    @DisplayName("Добавление списока проектов")
    public void addAllTest() throws Exception {
        int expectedNumberOfEntries = projectRepository.getSize() + 2;
        @NotNull final List<Project> projects = new ArrayList<>();
        @NotNull final String firstProjectName = "First Project Name";
        @NotNull final String firstProjectDescription = "Project Description";
        @NotNull final Project firstProject = new Project();
        firstProject.setName(firstProjectName);
        firstProject.setDescription(firstProjectDescription);
        firstProject.setUserId(USER_1_ID);
        projects.add(firstProject);
        @NotNull final String secondProjectName = "Second Project Name";
        @NotNull final String secondProjectDescription = "Project Description";
        @NotNull final Project secondProject = new Project();
        secondProject.setName(secondProjectName);
        secondProject.setDescription(secondProjectDescription);
        secondProject.setUserId(USER_1_ID);
        projects.add(secondProject);
        @NotNull final Collection<Project> addedProjects = projectRepository.add(projects);
        Assert.assertTrue(addedProjects.size() > 0);
        int actualNumberOfEntries = projectRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    @DisplayName("Удаление проектов для пользователя")
    public void clearForUserTest() throws Exception {
        int expectedNumberOfEntries = 0;
        projectRepository.clear(USER_1_ID);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER_1_ID));
    }

    @Test
    @DisplayName("Проверка существования проекта по Id")
    public void existByIdTrueTest() throws Exception {
        boolean expectedResult = true;
        @Nullable final Project project = projectRepository.findOneByIndex(1);
        Assert.assertNotNull(project);
        @Nullable final String projectId = project.getId();
        Assert.assertEquals(expectedResult, projectRepository.existsById(projectId));
    }

    @Test
    @DisplayName("Проверка несуществования проекта по Id")
    public void existByIdFalseTest() throws Exception {
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, projectRepository.existsById("qwerty"));
    }

    @Test
    @DisplayName("Проверка существования проекта по Id для пользователя")
    public void existByIdForUserTrueTest() throws Exception {
        boolean expectedResult = true;
        @Nullable final Project project = projectRepository.findOneByIndex(USER_1_ID, 1);
        Assert.assertNotNull(project);
        @Nullable final String projectId = project.getId();
        Assert.assertEquals(expectedResult, projectRepository.existsById(USER_1_ID, projectId));
    }

    @Test
    @DisplayName("Проверка несуществования проекта по Id для пользователя")
    public void existByIdForUserFalseTest() throws Exception {
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, projectRepository.existsById(USER_1_ID, "qwerty"));
    }

    @Test
    @DisplayName("Поиск всех проектов")
    public void findAllTest() throws Exception {
        @NotNull int allProjectsSize = projectRepository.findAll().size();
        @NotNull final List<Project> projects = projectRepository.findAll();
        Assert.assertEquals(allProjectsSize, projects.size());
    }

    @Test
    @DisplayName("Поиск всех проектов по компаратору")
    public void findAllWithComparatorTest() throws Exception {
        @NotNull Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        int allProjectsSize = projectRepository.findAll().size();
        @NotNull List<Project> projects = projectRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, projects.size());
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, projects.size());
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectRepository.findAll(comparator);
        Assert.assertEquals(allProjectsSize, projects.size());
    }

    @Test
    @DisplayName("Поиск всех проектов для пользователя")
    public void findAllForUserTest() throws Exception {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Project> projects = projectRepository.findAll(USER_1_ID);
        Assert.assertEquals(projectListForUser.size(), projects.size());
    }

    @Test
    @DisplayName("Поиск всех проектов по компаратору для пользователя")
    public void findAllWithComparatorForUser() throws Exception {
        @NotNull final List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Project> projects = projectRepository.findAll(USER_1_ID, comparator);
        Assert.assertEquals(projectListForUser, projects);
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectRepository.findAll(USER_1_ID, comparator);
        Assert.assertEquals(projectListForUser, projects);
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectRepository.findAll(USER_1_ID, comparator);
        Assert.assertEquals(projectListForUser, projects);
    }

    @Test
    @DisplayName("Поиск проекта по Id")
    public void findOneByIdTest() throws Exception {
        @Nullable Project project;
        for (int i = 1; i <= projectList.size(); i++) {
            project = projectRepository.findOneByIndex(i);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project foundProject = projectRepository.findOneById(projectId);
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    @DisplayName("Поиск проекта по Null Id")
    public void findOneByIdNullTest() throws Exception {
        @Nullable final Project foundProject = projectRepository.findOneById("qwerty");
        Assert.assertNull(foundProject);
        @Nullable final Project foundProjectNull = projectRepository.findOneById(null);
        Assert.assertNull(foundProjectNull);
    }

    @Test
    @DisplayName("Поиск проекта по Id для пользователя")
    public void findOneByIdForUserTest() throws Exception {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : projectListForUser) {
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project foundProject = projectRepository.findOneById(USER_1_ID, projectId);
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    @DisplayName("Поиск проекта по Null Id для пользователя")
    public void findOneByIdNullForUserTest() throws Exception {
        @Nullable final Project foundProject = projectRepository.findOneById(USER_1_ID, "qwerty");
        Assert.assertNull(foundProject);
        @Nullable final Project foundProjectNull = projectRepository.findOneById(USER_1_ID, null);
        Assert.assertNull(foundProjectNull);
    }

    @Test
    @DisplayName("Поиск проекта по индексу")
    public void findOneByIndexTest() throws Exception {
        for (int i = 1; i <= projectList.size(); i++) {
            @Nullable final Project project = projectRepository.findOneByIndex(i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    @DisplayName("Поиск проекта по Null индексу")
    public void findOneByIndexNullTest() throws Exception {
        @Nullable final Project project = projectRepository.findOneByIndex(null);
        Assert.assertNull(project);
    }

    @Test
    @DisplayName("Поиск проекта по индексу для пользователя")
    public void findOneByIndexForUserTest() throws Exception {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= projectListForUser.size(); i++) {
            @Nullable final Project project = projectRepository.findOneByIndex(USER_1_ID, i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    @DisplayName("Поиск проекта по Null индексу для пользователя")
    public void findOneByIndexNullForUserText() throws Exception {
        @Nullable final Project project = projectRepository.findOneByIndex(USER_1_ID, null);
        Assert.assertNull(project);
    }

    @Test
    @DisplayName("Получить количество проектов")
    public void getSizeTest() throws Exception {
        int actualSize = projectRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    @DisplayName("Получить количество проектов для пользователя")
    public void getSizeForUserTest() throws Exception {
        int expectedSize = (int) projectList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        int actualSize = projectRepository.getSize(USER_1_ID);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удалить пустой список проектов")
    public void removeAllNullTest() throws Exception {
        int expectedNumberOfEntries = projectRepository.getSize();
        projectRepository.removeAll(null);
        int actualNumberOfEntries = projectRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    @DisplayName("Удалить проект")
    public void removeTest() throws Exception {
        @Nullable final Project project = projectList.get(0);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @Nullable final Project deletedProject = projectRepository.remove(project);
        Assert.assertNotNull(deletedProject);
        @Nullable final Project deletedProjectInRepository = projectRepository.findOneById(projectId);
        Assert.assertNull(deletedProjectInRepository);
    }

    @Test
    @DisplayName("Удалить Null проект")
    public void removeNullTest() throws Exception {
        @Nullable final Project project = projectRepository.remove(null);
        Assert.assertNull(project);
    }

    @Test
    @DisplayName("Удалить проект по Id")
    public void removeByIdTest() throws Exception {
        int index = projectList.size();
        while (index > 0) {
            @Nullable final Project project = projectList.get(index - 1);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project deletedProject = projectRepository.removeById(projectId);
            Assert.assertNotNull(deletedProject);
            @Nullable final Project deletedProjectInRepository = projectRepository.findOneById(projectId);
            Assert.assertNull(deletedProjectInRepository);
            index--;
        }
    }

    @Test
    @DisplayName("Удалить проект по Null Id")
    public void removeByIdNullTest() throws Exception {
        @Nullable final Project deletedProject = projectRepository.removeById("qwerty");
        Assert.assertNull(deletedProject);
        @Nullable final Project deletedProjectNull = projectRepository.removeById(null);
        Assert.assertNull(deletedProjectNull);
    }

    @Test
    @DisplayName("Удалить проект по Id для пользователя")
    public void removeByIdForUserTest() throws Exception {
        int index = (int) projectList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Project project = projectRepository.findOneByIndex(USER_1_ID, index);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project deletedProject = projectRepository.removeById(USER_1_ID, projectId);
            Assert.assertNotNull(deletedProject);
            @Nullable final Project deletedProjectInRepository = projectRepository.findOneById(USER_1_ID, projectId);
            Assert.assertNull(deletedProjectInRepository);
            index--;
        }
    }

    @Test
    @DisplayName("Удалить проект по Null Id для пользователя")
    public void removeByIdNullForUserTest() throws Exception {
        @Nullable final Project deletedProject = projectRepository.removeById(USER_1_ID, "qwerty");
        Assert.assertNull(deletedProject);
        @Nullable final Project deletedProjectNull = projectRepository.removeById(USER_1_ID, null);
        Assert.assertNull(deletedProjectNull);
    }

    @Test
    @DisplayName("Удалить проект по Null индексу")
    public void removeByIndexNullTest() throws Exception {
        @Nullable final Project deletedProject = projectRepository.removeByIndex(null);
        Assert.assertNull(deletedProject);
    }

    @Test
    @DisplayName("Удалить проект по индексу для пользователя")
    public void removeByIndexForUserTest() throws Exception {
        int index = (int) projectList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Project deletedProject = projectRepository.removeByIndex(USER_1_ID, index);
            Assert.assertNotNull(deletedProject);
            @NotNull final String projectId = deletedProject.getId();
            @Nullable final Project deletedProjectInRepository = projectRepository.findOneById(USER_1_ID, projectId);
            Assert.assertNull(deletedProjectInRepository);
            index--;
        }
    }

    @Test
    @DisplayName("Удалить проект по Null индексу для пользователя")
    public void removeByIndexNullForUserTest() throws Exception {
        @Nullable final Project deletedProject = projectRepository.removeByIndex(USER_1_ID, null);
        Assert.assertNull(deletedProject);
    }

}