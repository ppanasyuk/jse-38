package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project>, IProjectRepository {

    @NotNull
    Project changeProjectStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status) throws Exception;

    @NotNull
    Project changeProjectStatusByIndex(@NotNull String userId, @Nullable Integer index, @Nullable Status status) throws Exception;

    @NotNull
    Project create(@NotNull String userId, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    Project create(@NotNull String userId, @Nullable String name) throws Exception;

    @NotNull
    Project updateById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    Project updateByIndex(@NotNull String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws Exception;

}