package ru.t1.panasyuk.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.AbstractModel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<M extends AbstractModel> {

    interface IRepositoryOptional<M> {

        @NotNull
        Optional<M> findOneById(@Nullable String id) throws Exception;

        @NotNull
        Optional<M> findOneByIndex(@NotNull Integer index) throws Exception;

    }

    default IRepositoryOptional<M> optional() {
        return new IRepositoryOptional<M>() {
            @NotNull
            @Override
            public Optional<M> findOneById(@Nullable final String id) throws Exception {
                return Optional.ofNullable(IRepository.this.findOneById(id));
            }

            @NotNull
            @Override
            public Optional<M> findOneByIndex(@NotNull final Integer index) throws Exception {
                return Optional.ofNullable(IRepository.this.findOneByIndex(index));
            }
        };
    }

    @NotNull
    M add(@NotNull M model) throws Exception;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws Exception;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws Exception;

    void clear() throws Exception;

    boolean existsById(@Nullable String id) throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws Exception;

    @Nullable
    M findOneById(@Nullable String id) throws Exception;

    @Nullable
    M findOneByIndex(@Nullable Integer index) throws Exception;

    int getSize() throws Exception;

    @Nullable
    M remove(@Nullable M model) throws Exception;

    void removeAll(@Nullable Collection<M> collection) throws Exception;

    @Nullable
    M removeById(@Nullable String id) throws Exception;

    @Nullable
    M removeByIndex(@Nullable Integer index) throws Exception;

    public void update(@NotNull M model) throws Exception;

}