package ru.t1.panasyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IProjectTaskService;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.exception.field.ProjectIdEmptyException;
import ru.t1.panasyuk.tm.exception.field.TaskIdEmptyException;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.repository.ProjectRepository;
import ru.t1.panasyuk.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    final IConnectionService connectionService;

    public ProjectTaskService(
            @NotNull final IConnectionService connectionService
    ) {
        this.connectionService = connectionService;
    }

    @NotNull
    public Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    public ITaskRepository getTaskRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    public IProjectRepository getProjectRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    @Override
    public Task bindTaskToProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Task task;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
                @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
                boolean isExist = projectRepository.existsById(userId, projectId);
                if (!isExist) throw new ProjectNotFoundException();
                task = taskRepository.findOneById(userId, taskId);
                if (task == null) throw new TaskNotFoundException();
                task.setProjectId(projectId);
                taskRepository.update(task);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return task;
    }

    @Override
    public Project removeProjectById(@NotNull final String userId, @Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable final Project project;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
                @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
                boolean isExist = projectRepository.existsById(userId, projectId);
                if (!isExist) throw new ProjectNotFoundException();
                project = projectRepository.removeById(userId, projectId);
                taskRepository.removeAllByProjectId(userId, projectId);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return project;
    }

    @Override
    public Project removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final Project project;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
                if (index > projectRepository.getSize(userId)) throw new IndexIncorrectException();
                project = projectRepository.findOneByIndex(userId, index);
                if (project == null) throw new ProjectNotFoundException();
                removeProjectById(userId, project.getId());
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return project;
    }

    @Override
    public void clearProjects(@NotNull final String userId) throws Exception {
        @NotNull final List<Project> projects;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
                projects = projectRepository.findAll(userId);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        for (@NotNull final Project project : projects) removeProjectById(userId, project.getId());
    }

    @NotNull
    @Override
    public Task unbindTaskFromProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Task task;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
                @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
                boolean isExist = projectRepository.existsById(userId, projectId);
                if (!isExist) throw new ProjectNotFoundException();
                task = taskRepository.findOneById(userId, taskId);
                if (task == null) throw new TaskNotFoundException();
                task.setProjectId(null);
                taskRepository.update(task);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return task;
    }

}