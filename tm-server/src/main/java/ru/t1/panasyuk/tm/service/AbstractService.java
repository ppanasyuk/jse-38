package ru.t1.panasyuk.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.IRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IService;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>>
        implements IService<M> {

    @NotNull
    final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    public abstract IRepository<M> getRepository(@NotNull final Connection connection);

    @NotNull
    public Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    @Override
    @SneakyThrows
    public M add(@NotNull final M model) throws Exception {
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                repository.add(model);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) throws Exception {
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                repository.add(models);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) throws Exception {
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                repository.set(models);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return models;
    }

    @Override
    public void clear() throws Exception {
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                repository.clear();
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        boolean result;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                result = repository.existsById(id);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll() throws Exception {
        @NotNull List<M> result;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                result = repository.findAll();
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws Exception {
        if (comparator == null) return findAll();
        @NotNull List<M> result;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                result = repository.findAll(comparator);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @Nullable M model;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                model = repository.findOneById(id);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return model;
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize()) throw new IndexIncorrectException();
        @Nullable M model;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                model = repository.findOneByIndex(index);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return model;
    }

    @Override
    public int getSize() throws Exception {
        int result;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                result = repository.getSize();
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    public M remove(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        @Nullable final M removedModel;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                removedModel = repository.remove(model);
                if (removedModel == null) throw new EntityNotFoundException();
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return removedModel;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) throws Exception {
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                repository.removeAll(collection);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

    @NotNull
    @Override
    public M removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M removedModel;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                removedModel = repository.removeById(id);
                if (removedModel == null) throw new EntityNotFoundException();
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return removedModel;
    }

    @NotNull
    @Override
    public M removeByIndex(@Nullable final Integer index) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable final M removedModel;
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                removedModel = repository.removeByIndex(index);
                if (removedModel == null) throw new EntityNotFoundException();
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return removedModel;
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<M> findAll(@Nullable final Sort sort) throws Exception {
        if (sort == null) return findAll();
        final Comparator<M> comparator = sort.getComparator();
        return findAll(comparator);
    }

    @Override
    public void update(@NotNull final M model) throws Exception {
        try (@NotNull final Connection connection = getConnection()) {
            try {
                @NotNull final IRepository<M> repository = getRepository(connection);
                repository.update(model);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

}