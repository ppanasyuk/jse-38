package ru.t1.panasyuk.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.AbstractUserOwnedModel;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @Nullable
    M add(@NotNull String userId, @Nullable M model) throws Exception;

    void clear(@NotNull String userId) throws Exception;

    boolean existsById(@NotNull String userId, @Nullable String id) throws Exception;

    @NotNull
    List<M> findAll(@NotNull String userId) throws Exception;

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable Comparator<M> comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String userId, @Nullable String id) throws Exception;

    @Nullable
    M findOneByIndex(@NotNull String userId, @Nullable Integer index) throws Exception;

    int getSize(@NotNull String userId) throws Exception;

    @Nullable
    M removeById(@NotNull String userId, @Nullable String id) throws Exception;

    @Nullable
    M removeByIndex(@NotNull String userId, @Nullable Integer index) throws Exception;

}