package ru.t1.panasyuk.tm.service;

import lombok.SneakyThrows;
import org.apache.commons.dbcp2.BasicDataSource;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final BasicDataSource dataSource;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        dataSource = dataSource();
    }

    public BasicDataSource dataSource() {
        @NotNull final BasicDataSource result = new BasicDataSource();
        @NotNull final String username = propertyService.getDBUser();
        @NotNull final String password = propertyService.getDBPassword();
        @NotNull final String url = propertyService.getDBUrl();
        result.setUrl(url);
        result.setUsername(username);
        result.setPassword(password);
        result.setMinIdle(5);
        result.setMaxIdle(10);
        result.setAutoCommitOnReturn(false);
        result.setMaxOpenPreparedStatements(100);
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        Connection connection = dataSource.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

}