package ru.t1.panasyuk.tm.service;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.model.User;

import java.util.*;
import java.util.stream.Collectors;

@DisplayName("Тестирование сервиса TaskService")
public class TaskServiceTest {

    @NotNull
    private List<Task> taskList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @NotNull
    private User test;

    @NotNull
    private User admin;

    @NotNull
    private static IConnectionService connectionService;

    @BeforeClass
    public static void initConnection() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() throws Exception {
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(connectionService);
        taskService = new TaskService(connectionService);
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);
        userService = new UserService(propertyService, connectionService, projectTaskService);
        taskList = new ArrayList<>();
        test = userService.create("TEST", "TEST", "TEST@TEST.ru");
        admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        @NotNull final Project project1 = projectService.create(test.getId(), "Project 1", "Project for TEST");
        @NotNull final Project project2 = projectService.create(admin.getId(), "Project 2", "Project for ADMIN");
        @NotNull final Task task1 = taskService.create(test.getId(), "Task 1", "Task for project 1");
        task1.setProjectId(project1.getId());
        taskService.update(task1);
        @NotNull final Task task3 = taskService.create(admin.getId(), "Task 3", "Task for project 2");
        task3.setProjectId(project2.getId());
        taskService.update(task3);
        taskList.add(task1);
        taskList.add(task3);
    }

    @After
    public void afterTest() throws Exception {
        userService.remove(admin);
        userService.remove(test);
    }

    @Test
    @DisplayName("Добавление задачи")
    public void AddTest() throws Exception {
        int expectedNumberOfEntries = taskService.getSize() + 1;
        @NotNull final Task task = new Task();
        task.setUserId(test.getId());
        task.setName("Test Add");
        task.setDescription("Test Add");
        taskService.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
        taskService.remove(task);
    }

    @Test
    @DisplayName("Добавление задачи с проверкой пользователя")
    public void AddForUserTest() throws Exception {
        int expectedNumberOfEntries = taskService.getSize(test.getId()) + 1;
        @NotNull final Task task = new Task();
        task.setUserId(test.getId());
        task.setName("Test Add");
        task.setDescription("Test Add");
        taskService.add(test.getId(), task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Добавление Null задачи с проверкой пользователя")
    public void AddNullForUserTest() throws Exception {
        int expectedNumberOfEntries = taskService.getSize(test.getId());
        @Nullable final Task task = taskService.add(test.getId(), null);
        Assert.assertNull(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Добавление списка задач")
    public void AddCollectionTest() throws Exception {
        int expectedNumberOfEntries = taskService.getSize() + 2;
        @NotNull final List<Task> taskList = new ArrayList<>();
        @NotNull final Task firstTask = new Task();
        firstTask.setUserId(test.getId());
        firstTask.setName("Test Add 1");
        firstTask.setDescription("Test Add 2");
        taskList.add(firstTask);
        @NotNull final Task secondTask = new Task();
        secondTask.setUserId(test.getId());
        secondTask.setName("Test Add 3");
        secondTask.setDescription("Test Add 4");
        taskList.add(secondTask);
        taskService.add(taskList);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize());
    }

    @Test
    @DisplayName("Изменение статуса задачи по Id")
    public void changeTaskStatusByIdTest() throws Exception {
        @NotNull final List<Task> tasks = taskService.findAll(test.getId());
        for (@NotNull final Task task : tasks) {
            @NotNull final String taskId = task.getId();
            @Nullable Task changedTask = taskService.changeTaskStatusById(test.getId(), taskId, Status.IN_PROGRESS);
            Assert.assertNotNull(changedTask);
            changedTask = taskService.findOneById(taskId);
            Assert.assertNotNull(changedTask);
            Assert.assertEquals(Status.IN_PROGRESS, changedTask.getStatus());
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Изменение статуса задачи по пустому Id")
    public void changeTaskStatusByIdTaskIdEmptyTestNegative() throws Exception {
        @Nullable Task changedTask = taskService.changeTaskStatusById(test.getId(), "", Status.IN_PROGRESS);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Изменение статуса задачи по Null Id")
    public void changeTaskStatusByIdNullTaskIdEmptyTestNegative() throws Exception {
        @Nullable Task changedTask = taskService.changeTaskStatusById(test.getId(), null, Status.IN_PROGRESS);
    }

    @Test(expected = StatusIncorrectException.class)
    @DisplayName("Изменение статуса задачи по Id на некорректный")
    public void changeTaskStatusByIdStatusIncorrectTestNegative() throws Exception {
        @Nullable Task changedTask = taskService.changeTaskStatusById(test.getId(), "123", null);
    }

    @Test(expected = TaskNotFoundException.class)
    @DisplayName("Изменение статуса несуществующей")
    public void changeTaskStatusByIdTaskNotFoundTestNegative() throws Exception {
        @Nullable Task changedTask = taskService.changeTaskStatusById(test.getId(), "123", Status.IN_PROGRESS);
    }

    @Test
    @DisplayName("Изменение статуса задачи по индексу")
    public void changeTaskStatusByIndexTest() throws Exception {
        @NotNull final List<Task> tasks = taskService.findAll(test.getId());
        for (int i = 0; i < tasks.size(); i++) {
            @NotNull final Task task = tasks.get(i);
            @NotNull final String taskId = task.getId();
            @Nullable Task changedTask = taskService.changeTaskStatusByIndex(test.getId(), i + 1, Status.IN_PROGRESS);
            Assert.assertNotNull(changedTask);
            changedTask = taskService.findOneById(taskId);
            Assert.assertNotNull(changedTask);
            Assert.assertEquals(Status.IN_PROGRESS, changedTask.getStatus());
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Изменение статуса задачи по Null индексу")
    public void changeTaskStatusByIndexIndexNullTestNegative() throws Exception {
        @Nullable Task changedTask = taskService.changeTaskStatusByIndex(test.getId(), null, Status.IN_PROGRESS);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Изменение статуса задачи по отрицательному индексу")
    public void changeTaskStatusByIndexIndexMinusTestNegative() throws Exception {
        @Nullable Task changedTask = taskService.changeTaskStatusByIndex(test.getId(), -1, Status.IN_PROGRESS);
    }

    @Test(expected = StatusIncorrectException.class)
    @DisplayName("Изменение статуса задачи по индексу на Null")
    public void changeTaskStatusByIIndexStatusIncorrectTestNegative() throws Exception {
        @Nullable Task changedTask = taskService.changeTaskStatusByIndex(test.getId(), 1, null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Изменение статуса задачи по индексу больше количества задач")
    public void changeTaskStatusByIndexIndexIncorrectTestNegative() throws Exception {
        @Nullable Task changedTask = taskService.changeTaskStatusByIndex(test.getId(), 100, Status.IN_PROGRESS);
    }

    @Test
    @DisplayName("Удаление всех задач пользователя")
    public void clearForUserTest() throws Exception {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(taskService.getSize(test.getId()) > 0);
        taskService.clear(test.getId());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Создание задачи с именем и описанием")
    public void createTest() throws Exception {
        int expectedNumberOfEntries = taskService.getSize(test.getId()) + 1;
        @NotNull final String name = "Task name";
        @NotNull final String description = "Task Description";
        @Nullable Task createdTask = taskService.create(test.getId(), name, description);
        @NotNull final String taskId = createdTask.getId();
        Assert.assertNotNull(createdTask);
        createdTask = taskService.findOneById(test.getId(), taskId);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(name, createdTask.getName());
        Assert.assertEquals(description, createdTask.getDescription());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
        taskService.remove(createdTask);
    }

    @Test
    @DisplayName("Создание задачи с именем")
    public void createByNameTest() throws Exception {
        int expectedNumberOfEntries = taskService.getSize(test.getId()) + 1;
        @NotNull final String name = "Task name";
        @Nullable Task createdTask = taskService.create(test.getId(), name);
        @NotNull final String taskId = createdTask.getId();
        Assert.assertNotNull(createdTask);
        createdTask = taskService.findOneById(test.getId(), taskId);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(name, createdTask.getName());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
        taskService.remove(createdTask);
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создание задачи с пустым именем и описанием")
    public void createNameEmptyTestNegative() throws Exception {
        taskService.create(test.getId(), "", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создание задачи с Null именем и описанием")
    public void createNullNameEmptyTestNegative() throws Exception {
        taskService.create(test.getId(), null, "description");
    }

    @Test(expected = DescriptionEmptyException.class)
    @DisplayName("Создание задачи с пустым описанием")
    public void createDescriptionEmptyTestNegative() throws Exception {
        taskService.create(test.getId(), "name", "");
    }

    @Test(expected = DescriptionEmptyException.class)
    @DisplayName("Создание задачи с Null описанием")
    public void createNullDescriptionEmptyTestNegative() throws Exception {
        taskService.create(test.getId(), "name", null);
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создание задачи с пустым именем")
    public void createByNameNameEmptyTestNegative() throws Exception {
        taskService.create(test.getId(), "");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создание задачи с Null именем")
    public void createByNameNullNameEmptyTestNegative() throws Exception {
        taskService.create(test.getId(), null);
    }

    @Test
    @DisplayName("Проверка существования задачи по Id")
    public void existByIdTrueTest() throws Exception {
        for (@NotNull final Task task : taskList) {
            final boolean isExist = taskService.existsById(task.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    @DisplayName("Проверка несуществования задачи по Id")
    public void existByIdFalseTest() throws Exception {
        final boolean isExist = taskService.existsById("123321");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Проверка существования задачи по Id для пользователя")
    public void existByIdTrueForUserTest() throws Exception {
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Task task : tasksForTestUser) {
            final boolean isExist = taskService.existsById(test.getId(), task.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    @DisplayName("Проверка несуществования задачи по Id для пользователя")
    public void existByIdFalseUserTest() throws Exception {
        final boolean isExist = taskService.existsById("45", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Найти все задачи по Id проекта")
    public void findAllByProjectIdTest() throws Exception {
        @Nullable final Project project = projectService.findOneByIndex(test.getId(), 1);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @NotNull final List<Task> tasksToFind = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
        @NotNull final List<Task> tasksByProjectId = taskService.findAllByProjectId(test.getId(), projectId);
        Assert.assertEquals(tasksToFind, tasksByProjectId);
    }

    @Test
    @DisplayName("Найти все задачи по пустому Id проекта")
    public void findAllByProjectIdEmptyTest() throws Exception {
        @NotNull final List<Task> emptyList = Collections.emptyList();
        @NotNull final List<Task> tasksByProjectId = taskService.findAllByProjectId(test.getId(), "");
        Assert.assertEquals(emptyList, tasksByProjectId);
    }

    @Test
    @DisplayName("Найти все задачи по Null Id проекта")
    public void findAllByProjectIdNullTest() throws Exception {
        @NotNull final List<Task> emptyList = Collections.emptyList();
        @NotNull final List<Task> tasksByProjectId = taskService.findAllByProjectId(test.getId(), null);
        Assert.assertEquals(emptyList, tasksByProjectId);
    }

    @Test
    @DisplayName("Найти все задачи")
    public void findAllTest() throws Exception {
        @NotNull final List<Task> tasks = taskService.findAll();
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    @DisplayName("Найти все задачи для пользователя")
    public void findAllForUserTest() throws Exception {
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Task> tasks = taskService.findAll(test.getId());
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
    }

    @Test
    @DisplayName("Найти все задачи с компаратором")
    public void findAllWithComparatorTest() throws Exception {
        @Nullable Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        int countOfRecords = taskService.getSize();
        @NotNull List<Task> tasks = taskService.findAll(comparator);
        Assert.assertEquals(countOfRecords, tasks.size());
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskService.findAll(comparator);
        Assert.assertEquals(countOfRecords, tasks.size());
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskService.findAll(comparator);
        Assert.assertEquals(countOfRecords, tasks.size());
        comparator = null;
        tasks = taskService.findAll(comparator);
        Assert.assertEquals(countOfRecords, tasks.size());
    }

    @Test
    @DisplayName("Найти все задачи с компаратором для пользователей")
    public void findAllWithComparatorForUserTest() throws Exception {
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Task> tasks = taskService.findAll(test.getId(), comparator);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskService.findAll(test.getId(), comparator);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskService.findAll(test.getId(), comparator);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
    }

    @Test
    @DisplayName("Найти все задачи с сортировкой")
    public void findAllWithSortTest() throws Exception {
        int countOfRecords = taskService.getSize();
        @NotNull List<Task> tasks = taskService.findAll(Sort.BY_NAME);
        Assert.assertEquals(countOfRecords, tasks.size());
        tasks = taskService.findAll(Sort.BY_CREATED);
        Assert.assertEquals(countOfRecords, tasks.size());
        tasks = taskService.findAll(Sort.BY_STATUS);
        Assert.assertEquals(countOfRecords, tasks.size());
        @Nullable final Sort sort = null;
        tasks = taskService.findAll(sort);
        Assert.assertEquals(countOfRecords, tasks.size());
    }

    @Test
    @DisplayName("Найти все задачи с сортировкой для пользователей")
    public void findAllWithSortForUserTest() throws Exception {
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull List<Task> tasks = taskService.findAll(test.getId(), Sort.BY_NAME);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        tasks = taskService.findAll(test.getId(), Sort.BY_CREATED);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        tasks = taskService.findAll(test.getId(), Sort.BY_STATUS);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        @Nullable final Sort sort = null;
        tasks = taskService.findAll(test.getId(), sort);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
    }

    @Test
    @DisplayName("Найти задачу по Id")
    public void findOneByIdTest() throws Exception {
        @Nullable Task foundTask;
        for (@NotNull final Task task : taskList) {
            foundTask = taskService.findOneById(task.getId());
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    @DisplayName("Найти задачу по Id для пользователя")
    public void findOneByIdForUserTest() throws Exception {
        @Nullable Task foundTask;
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Task task : tasksForTestUser) {
            foundTask = taskService.findOneById(test.getId(), task.getId());
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    @DisplayName("Найти задачу по Null Id")
    public void findOneByIdNullTest() throws Exception {
        @Nullable final Task foundTask = taskService.findOneById(null);
        Assert.assertNull(foundTask);
    }

    @Test
    @DisplayName("Найти задачу по пустому Id")
    public void findOneByIdEmptyTest() throws Exception {
        @Nullable final Task foundTask = taskService.findOneById("");
        Assert.assertNull(foundTask);
    }

    @Test
    @DisplayName("Найти задачу по Null Id для пользователя")
    public void findOneByIdNullForUserTest() throws Exception {
        @Nullable final Task foundTask = taskService.findOneById(test.getId(), null);
        Assert.assertNull(foundTask);
    }

    @Test
    @DisplayName("Найти задачу по индексу")
    public void findOneByIndexTest() throws Exception {
        for (int i = 1; i <= taskList.size(); i++) {
            @Nullable final Task task = taskService.findOneByIndex(i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    @DisplayName("Найти задачу по индексу для пользователя")
    public void findOneByIndexForUserTest() throws Exception {
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= tasksForTestUser.size(); i++) {
            @Nullable final Task task = taskService.findOneByIndex(test.getId(), i);
            Assert.assertNotNull(task);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по индексу превышающему количество задач для пользователя")
    public void findOneByIndexForUserIndexIncorrectNegative() throws Exception {
        int index = taskService.getSize(test.getId()) + 1;
        @Nullable final Task task = taskService.findOneByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по Null индексу для пользователя")
    public void findOneByIndexForUserNullIndexIncorrectNegative() throws Exception {
        @Nullable final Task task = taskService.findOneByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по отрицательному индексу для пользователя")
    public void findOneByIndexForUserMinusIndexIncorrectNegative() throws Exception {
        @Nullable final Task task = taskService.findOneByIndex(test.getId(), -1);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по индексу превышающему количество задач")
    public void findOneByIndexIndexIncorrectNegative() throws Exception {
        int index = taskService.getSize() + 1;
        @Nullable final Task task = taskService.findOneByIndex(index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по Null индексу")
    public void findOneByIndexNullIndexIncorrectNegative() throws Exception {
        @Nullable final Task task = taskService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по отрицательному индексу")
    public void findOneByIndexMinusIndexIncorrectNegative() throws Exception {
        @Nullable final Task task = taskService.findOneByIndex(-1);
    }

    @Test
    @DisplayName("Получить количество задач")
    public void getSizeTest() throws Exception {
        int actualSize = taskService.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    @DisplayName("Получить количество задач для пользователя")
    public void getSizeForUserTest() throws Exception {
        int expectedSize = (int) taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .count();
        int actualSize = taskService.getSize(test.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удалить задачу")
    public void removeTest() throws Exception {
        for (@NotNull final Task task : taskList) {
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskService.remove(task);
            Assert.assertNotNull(deletedTask);
            @Nullable final Task deletedTaskInRepository = taskService.findOneById(taskId);
            Assert.assertNull(deletedTaskInRepository);
        }
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить Null задачу")
    public void removeEntityNullNotFoundTestNegative() throws Exception {
        @Nullable final Task deletedTask = taskService.remove(null);
    }

    @Test
    @DisplayName("Удалить все задачи методом removeAll")
    public void removeAllTest() throws Exception {
        int expectedNumberOfEntries = 0;
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        taskService.removeAll(tasksForTestUser);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Удалить задачу по Id")
    public void removeByIdTest() throws Exception {
        for (@NotNull final Task task : taskList) {
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskService.removeById(taskId);
            Assert.assertNotNull(deletedTask);
            @Nullable final Task deletedTaskInRepository = taskService.findOneById(taskId);
            Assert.assertNull(deletedTaskInRepository);
        }
    }

    @Test
    @DisplayName("Удалить задачу по Id для пользователя")
    public void removeByIdForUserTest() throws Exception {
        @NotNull final List<Task> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Task task : tasksForTestUser) {
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskService.removeById(test.getId(), taskId);
            Assert.assertNotNull(deletedTask);
            @Nullable final Task deletedTaskInRepository = taskService.findOneById(test.getId(), taskId);
            Assert.assertNull(deletedTaskInRepository);
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить задачу по Null Id")
    public void removeByIdIdNullTestNegative() throws Exception {
        taskService.removeById(null);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить задачу по пустому Id")
    public void removeByIdIdEmptyTestNegative() throws Exception {
        taskService.removeById("");
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить несуществующую")
    public void removeByIdEntityNotFoundTestNegative() throws Exception {
        taskService.removeById("123321");
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить задачу по Null Id для пользователя")
    public void removeByIdForUserIdNullTestNegative() throws Exception {
        taskService.removeById(test.getId(), null);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить задачу по пустому Id для пользователя")
    public void removeByIdForUserIdEmptyTestNegative() throws Exception {
        taskService.removeById(test.getId(), "");
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить несуществующую задачу по Id для пользователя")
    public void removeByIdForUserEntityNotFoundTestNegative() throws Exception {
        taskService.removeById(test.getId(), "123321");
    }

    @Test
    @DisplayName("Удалить задачу по индексу для пользователя")
    public void removeByIndexForUserTest() throws Exception {
        int index = (int) taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Task deletedTask = taskService.removeByIndex(test.getId(), index);
            Assert.assertNotNull(deletedTask);
            @NotNull final String taskId = deletedTask.getId();
            @Nullable final Task deletedTaskInRepository = taskService.findOneById(test.getId(), taskId);
            Assert.assertNull(deletedTaskInRepository);
            index--;
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить задачу по Null индексу")
    public void removeByIndexNullIndexIncorrectTestNegative() throws Exception {
        taskService.removeByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить задачу по отрицательному индексу ноля")
    public void removeByIndexMinusIndexIncorrectTestNegative() throws Exception {
        taskService.removeByIndex(-1);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить задачу по индексу больше количества задач для пользователя")
    public void removeByIndexForUserIndexIncorrectTestNegative() throws Exception {
        int index = taskList.size() + 1;
        taskService.removeByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить задачу по Null индексу для пользователя")
    public void removeByIndexNullForUserIndexIncorrectTestNegative() throws Exception {
        taskService.removeByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить задачу по отрицательному индексу для пользователя")
    public void removeByIndexMinusForUserIndexIncorrectTestNegative() throws Exception {
        taskService.removeByIndex(test.getId(), -1);
    }

    @Test
    @DisplayName("Обновить задачу по Id")
    public void updateByIdTest() throws Exception {
        @NotNull final List<Task> tasks = taskService.findAll(test.getId());
        @NotNull String name = "";
        @NotNull String description = "";
        int index = 0;
        for (@NotNull final Task task : tasks) {
            @NotNull final String taskId = task.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable Task updatedTask = taskService.updateById(test.getId(), taskId, name, description);
            Assert.assertNotNull(updatedTask);
            updatedTask = taskService.findOneById(taskId);
            Assert.assertNotNull(updatedTask);
            Assert.assertEquals(name, updatedTask.getName());
            Assert.assertEquals(description, updatedTask.getDescription());
            index++;
        }
    }

    @Test
    @DisplayName("Обновить задачу по индексу")
    public void updateByIndexTest() throws Exception {
        @NotNull final List<Task> tasks = taskService.findAll(test.getId());
        @NotNull String name = "";
        @NotNull String description = "";
        int index = 1;
        for (@NotNull final Task task : tasks) {
            @NotNull final String taskId = task.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable Task updatedTask = taskService.updateByIndex(test.getId(), index, name, description);
            Assert.assertNotNull(updatedTask);
            updatedTask = taskService.findOneById(taskId);
            Assert.assertNotNull(updatedTask);
            Assert.assertEquals(name, updatedTask.getName());
            Assert.assertEquals(description, updatedTask.getDescription());
            index++;
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Обновить задачу по пустому Id")
    public void UpdateByIdIdEmptyTestNegative() throws Exception {
        taskService.updateById(test.getId(), "", "name", "description");
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Обновить задачу по Null Id")
    public void UpdateByIdNullIdEmptyTestNegative() throws Exception {
        taskService.updateById(test.getId(), null, "name", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновить задачу по Id и пустым именем")
    public void UpdateByIdNameEmptyTestNegative() throws Exception {
        taskService.updateById(test.getId(), "id", "", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновить задачу по Id и Null именем")
    public void UpdateByIdNullNameEmptyTestNegative() throws Exception {
        taskService.updateById(test.getId(), "id", null, "description");
    }

    @Test(expected = TaskNotFoundException.class)
    @DisplayName("Обновить несуществующую задачу по Id")
    public void UpdateByIdTaskNotFoundTestNegative() throws Exception {
        taskService.updateById(test.getId(), "123", "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Обновить задачу по Null индексу")
    public void UpdateByIndexIndexNullTestNegative() throws Exception {
        taskService.updateByIndex(test.getId(), null, "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Обновить задачу по отрицательному индексу")
    public void UpdateByIndexMinusTestNegative() throws Exception {
        taskService.updateByIndex(test.getId(), -1, "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Обновить задачу по индексу больше количества задач")
    public void UpdateByIndexIndexIncorrectTestNegative() throws Exception {
        taskService.updateByIndex(test.getId(), 100, "", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновить задачу по индексу с Null именем")
    public void UpdateByIndexNullNameEmptyTestNegative() throws Exception {
        taskService.updateByIndex(test.getId(), 1, null, "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновить задачу по индексу с пустым именем")
    public void UpdateByIndexNameEmptyTestNegative() throws Exception {
        taskService.updateByIndex(test.getId(), 1, "", "description");
    }

}