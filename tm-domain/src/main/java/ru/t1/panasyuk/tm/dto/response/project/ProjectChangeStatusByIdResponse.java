package ru.t1.panasyuk.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectChangeStatusByIdResponse extends AbstractProjectResponse {

    @Nullable
    private Project project;

    public ProjectChangeStatusByIdResponse(@Nullable final Project project) {
        this.project = project;
    }

    public ProjectChangeStatusByIdResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}