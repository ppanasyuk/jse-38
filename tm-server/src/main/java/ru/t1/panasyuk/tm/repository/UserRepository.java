package ru.t1.panasyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.constant.DBConst;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.model.User;

import java.sql.*;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConst.TABLE_USER;
    }

    @NotNull
    @Override
    protected String getColumns() {
        return String.format(
                "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s",
                DBConst.COLUMN_ID,
                DBConst.COLUMN_LOGIN,
                DBConst.COLUMN_PASSWORD,
                DBConst.COLUMN_FIRST_NAME,
                DBConst.COLUMN_LAST_NAME,
                DBConst.COLUMN_MIDDLE_NAME,
                DBConst.COLUMN_EMAIL,
                DBConst.COLUMN_ROLE,
                DBConst.COLUMN_LOCKED,
                DBConst.COLUMN_CREATED
        );
    }

    @NotNull
    @Override
    public User add(@NotNull final User user) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                DBConst.TABLE_USER,
                getColumns()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getEmail());
            statement.setString(8, user.getRole().toString());
            statement.setBoolean(9, user.getLocked());
            statement.setTimestamp(10, new Timestamp(user.getCreated().getTime()));
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @Override
    public User fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final User user = new User();
        user.setId(row.getString(DBConst.COLUMN_ID));
        user.setLogin(row.getString(DBConst.COLUMN_LOGIN));
        user.setPasswordHash(row.getString(DBConst.COLUMN_PASSWORD));
        user.setFirstName(row.getString(DBConst.COLUMN_FIRST_NAME));
        user.setLastName(row.getString(DBConst.COLUMN_LAST_NAME));
        user.setMiddleName(row.getString(DBConst.COLUMN_MIDDLE_NAME));
        user.setEmail(row.getString(DBConst.COLUMN_EMAIL));
        user.setRole(Role.valueOf(row.getString(DBConst.COLUMN_ROLE)));
        user.setLocked(row.getBoolean(DBConst.COLUMN_LOCKED));
        user.setCreated(row.getTimestamp(DBConst.COLUMN_CREATED));
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null) return null;
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(),
                DBConst.COLUMN_LOGIN
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) throws Exception {
        if (email == null) return null;
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(),
                DBConst.COLUMN_EMAIL
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) throws Exception {
        if (email == null) return false;
        return findByEmail(email) != null;
    }

    @Override
    public void update(@NotNull final User user) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(),
                DBConst.COLUMN_LOGIN,
                DBConst.COLUMN_PASSWORD,
                DBConst.COLUMN_EMAIL,
                DBConst.COLUMN_FIRST_NAME,
                DBConst.COLUMN_LAST_NAME,
                DBConst.COLUMN_MIDDLE_NAME,
                DBConst.COLUMN_LOCKED,
                DBConst.COLUMN_ROLE,
                DBConst.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getMiddleName());
            statement.setBoolean(7, user.getLocked());
            statement.setString(8, user.getRole().toString());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
    }

}