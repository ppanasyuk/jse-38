package ru.t1.panasyuk.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListResponse extends AbstractTaskResponse {

    @Nullable
    private List<Task> tasks;

    public TaskListResponse(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

    public TaskListResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}