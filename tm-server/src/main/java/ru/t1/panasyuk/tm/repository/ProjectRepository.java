package ru.t1.panasyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.constant.DBConst;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.model.Project;

import java.sql.*;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConst.TABLE_PROJECT;
    }

    @NotNull
    @Override
    protected String getColumns() {
        return String.format(
                "%s, %s, %s, %s, %s, %s",
                DBConst.COLUMN_ID,
                DBConst.COLUMN_NAME,
                DBConst.COLUMN_CREATED,
                DBConst.COLUMN_STATUS,
                DBConst.COLUMN_DESCRIPTION,
                DBConst.COLUMN_USER_ID
        );
    }

    @NotNull
    @Override
    public Project add(@NotNull final Project project) throws SQLException {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s) VALUES(?, ?, ?, ?, ?, ?)",
                getTableName(),
                getColumns()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getName());
            statement.setTimestamp(3, new Timestamp(project.getCreated().getTime()));
            statement.setString(4, project.getStatus().toString());
            statement.setString(5, project.getDescription());
            statement.setString(6, project.getUserId());
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    public Project fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(row.getString(DBConst.COLUMN_ID));
        project.setName(row.getString(DBConst.COLUMN_NAME));
        project.setCreated(row.getTimestamp(DBConst.COLUMN_CREATED));
        project.setStatus(Status.valueOf(row.getString(DBConst.COLUMN_STATUS)));
        project.setDescription(row.getString(DBConst.COLUMN_DESCRIPTION));
        project.setUserId(row.getString(DBConst.COLUMN_USER_ID));
        return project;
    }

    @Override
    public void update(@NotNull final Project project) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(),
                DBConst.COLUMN_NAME,
                DBConst.COLUMN_DESCRIPTION,
                DBConst.COLUMN_STATUS,
                DBConst.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getStatus().toString());
            statement.setString(4, project.getId());
            statement.executeUpdate();
        }
    }

}