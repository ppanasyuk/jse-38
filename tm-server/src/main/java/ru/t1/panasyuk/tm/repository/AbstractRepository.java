package ru.t1.panasyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.IRepository;
import ru.t1.panasyuk.tm.comparator.CreatedComparator;
import ru.t1.panasyuk.tm.comparator.NameComparator;
import ru.t1.panasyuk.tm.comparator.StatusComparator;
import ru.t1.panasyuk.tm.constant.DBConst;
import ru.t1.panasyuk.tm.model.AbstractModel;

import java.sql.*;
import java.util.*;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected abstract String getColumns();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == NameComparator.INSTANCE) return DBConst.COLUMN_NAME;
        if (comparator == StatusComparator.INSTANCE) return DBConst.COLUMN_STATUS;
        return DBConst.COLUMN_CREATED;
    }

    protected AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    public abstract M fetch(@NotNull final ResultSet row) throws Exception;

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) throws Exception {
        for (@NotNull final M model : models)
            add(model);
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) throws Exception {
        clear();
        add(models);
        return models;
    }

    @Override
    public void clear() throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s ORDER BY %s DESC",
                getTableName(),
                DBConst.COLUMN_CREATED
        );
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        if (comparator == null) return findAll();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s ORDER BY %s",
                getTableName(),
                getSortType(comparator)
        );
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) throws Exception {
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1",
                getTableName(),
                DBConst.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) throws Exception {
        if (index == null) return null;
        @NotNull final String sql = String.format(
                "SELECT * FROM %s ORDER BY %s DESC LIMIT ?",
                getTableName(),
                DBConst.COLUMN_CREATED
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            int i = 0;
            while (i < index) {
                if (!resultSet.next()) return null;
                i++;
            }
            return fetch(resultSet);
        }
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final String sql = String.format(
                "SELECT COUNT(%s) FROM %s",
                DBConst.COLUMN_ID,
                getTableName()
        );
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();
            return resultSet.getInt(1);
        }
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) throws Exception {
        if (model == null) return null;
        @NotNull final String sql = String.format(
                "DELETE FROM %s WHERE %s = ?",
                getTableName(),
                DBConst.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) throws Exception {
        if (collection == null) return;
        for (@NotNull final M model : collection) {
            removeById(model.getId());
        }
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) throws Exception {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) throws Exception {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        return remove(model);
    }

}