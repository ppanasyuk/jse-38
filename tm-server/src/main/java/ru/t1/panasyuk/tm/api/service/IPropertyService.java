package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationLog();

    @NotNull
    String getApplicationName();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getDBUser();

    @NotNull
    String getDBPassword();

    @NotNull
    String getDBUrl();

    @NotNull
    String getGitBranch();

    @NotNull
    String getGitCommitId();

    @NotNull
    String getGitCommitterName();

    @NotNull
    String getGitCommitterEmail();

    @NotNull
    String getGitCommitMessage();

    @NotNull
    String getGitCommitTime();

    @NotNull
    String getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getSessionKey();

    int getSessionTimeout();

}