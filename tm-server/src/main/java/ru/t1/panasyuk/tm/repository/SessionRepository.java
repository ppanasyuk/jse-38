package ru.t1.panasyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.ISessionRepository;
import ru.t1.panasyuk.tm.constant.DBConst;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.model.Session;

import java.sql.*;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConst.TABLE_SESSION;
    }

    @NotNull
    @Override
    protected String getColumns() {
        return String.format(
                "%s, %s, %s, %s",
                DBConst.COLUMN_ID,
                DBConst.COLUMN_CREATED,
                DBConst.COLUMN_ROLE,
                DBConst.COLUMN_USER_ID
        );
    }

    @NotNull
    @Override
    public Session add(@NotNull final Session session) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s) VALUES(?, ?, ?, ?)",
                getTableName(),
                getColumns()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setTimestamp(2, new Timestamp(session.getCreated().getTime()));
            @Nullable final Role sessionRole = session.getRole();
            if (sessionRole == null) statement.setString(3, "");
            else statement.setString(3, session.getRole().toString());
            statement.setString(4, session.getUserId());
            statement.executeUpdate();
        }
        return session;
    }

    @NotNull
    @Override
    public Session fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Session session = new Session();
        session.setId(row.getString(DBConst.COLUMN_ID));
        session.setRole(Role.valueOf(row.getString(DBConst.COLUMN_ROLE)));
        session.setUserId(row.getString(DBConst.COLUMN_USER_ID));
        return session;
    }

    @Override
    public void update(@NotNull final Session session) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ? WHERE %s = ?",
                getTableName(),
                DBConst.COLUMN_ROLE,
                DBConst.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getRole().toString());
            statement.setString(2, session.getId());
            statement.executeUpdate();
        }
    }

}