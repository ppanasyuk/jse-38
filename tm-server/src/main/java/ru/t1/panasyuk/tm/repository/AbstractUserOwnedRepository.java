package ru.t1.panasyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.IUserOwnedRepository;
import ru.t1.panasyuk.tm.constant.DBConst;
import ru.t1.panasyuk.tm.model.AbstractUserOwnedModel;

import java.sql.*;
import java.util.*;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    protected AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public M add(@NotNull final String userId, @Nullable final M model) throws Exception {
        if (model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void clear(@NotNull final String userId) throws Exception {
        @NotNull final String sql = String.format(
                "DELETE FROM %s WHERE %s = ?",
                getTableName(),
                DBConst.COLUMN_USER_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) throws Exception {
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? ORDER BY %s DESC",
                getTableName(),
                DBConst.COLUMN_USER_ID,
                DBConst.COLUMN_CREATED
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        if (comparator == null) return findAll(userId);
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? ORDER BY %s",
                getTableName(),
                DBConst.COLUMN_USER_ID,
                getSortType(comparator)
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @Nullable final String id) throws Exception {
        if (id == null) return null;
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s =? AND %s = ? LIMIT 1",
                getTableName(),
                DBConst.COLUMN_USER_ID,
                DBConst.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @Nullable final Integer index) throws Exception {
        if (index == null) return null;
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? ORDER BY %s DESC LIMIT ?",
                getTableName(),
                DBConst.COLUMN_USER_ID,
                DBConst.COLUMN_CREATED
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            int i = 0;
            while (i < index) {
                if (!resultSet.next()) return null;
                i++;
            }
            return fetch(resultSet);
        }
    }

    @Override
    public int getSize(@NotNull final String userId) throws Exception {
        @NotNull final String sql = String.format(
                "SELECT COUNT(%s) FROM %s WHERE %s = ?",
                DBConst.COLUMN_ID,
                getTableName(),
                DBConst.COLUMN_USER_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt(1);
        }
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String userId, @Nullable final String id) throws Exception {
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final String userId, @Nullable final Integer index) throws Exception {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

}